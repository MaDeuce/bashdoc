#
# Examples of using bashdoc to document itself.
#

all: bashdoc.md bashdoc.pdf bashdoc.1 bashdoc.1.ps Readme.md

bashdoc.md: bashdoc
	./bashdoc md ./bashdoc > bashdoc.md

bashdoc.pdf: bashdoc.md
	./bashdoc pdf ./bashdoc > bashdoc.pdf

bashdoc.1: bashdoc.md
	./bashdoc man ./bashdoc > bashdoc.1

bashdoc.1.ps: bashdoc.md
	./bashdoc manps ./bashdoc > bashdoc.1.ps

# Bitbucket will automatically render a Readme.md
# file in the root of a project.  Bitbucket's 
# markdown engine can't handle pandoc's markdown.
# Create a safe version of the documentation for it.
Readme.md: bashdoc
	./bashdoc mds ./bashdoc > Readme.md

clean: tidy
	@rm -f *.md *.mds *.pdf *.1 *.ps

tidy:
	@rm -f *~
