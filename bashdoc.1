BASHDOC(1)                                                          BASHDOC(1)



NNAAMMEE
       bbaasshhddoocc - extract and format documentation embedded in bash scripts

SSYYNNOOPPSSIISS
       bbaasshhddoocc [--hh|--vv] _f_o_r_m_a_t _S_C_R_I_P_T

DDEESSCCRRIIPPTTIIOONN
       bbaasshhddoocc[1]  extracts  documentation  from  a bash script and formats it
       nicely.  The documentation in _S_C_R_I_P_T must be  written  in  markdown[2].
       It will be formatted for output using pandoc[3].

       For bbaasshhddoocc to be able to format a script's documentation, a simple but
       rigid convention must be followed.  Namely, documentation must be  con-
       tained in a 'here document' that begins with either:

               : <<'DOC'

       or

               : <<DOC

       and ends with:

               DOC

       In  all  cases,  the text must be _e_x_a_c_t_l_y as shown and it _m_u_s_t start in
       column 1.  If multiple such here documents are encountered in a script,
       they  are  concatenated  into a single documentation string before pro-
       cessing.

       Contents of the documentation string must be created with pandoc's fla-
       vor of markdown with the extension for definition lists enabled.

   AArrgguummeennttss
   RReeqquuiirreedd AArrgguummeennttss
       The  first  argument to bbaasshhddoocc is the output _f_o_r_m_a_t, which must be one
       of the following:

       mmaann    Extract the raw documentation from _S_C_R_I_P_T and convert  it  to  a
              man  page,  per  ggeettttrrooffff,  and  view the resulting with man(1).
              Note that most user environments are  typically  setup  so  that
              man(1) displays the documentation with a pager, such as lleessss.

       mmaannppss  Extract  the  raw  documentation from _S_C_R_I_P_T and process it with
              pandoc and man(1) to create a postscript version of the man page
              generated  by  mmaann.  When used in this manner, man(1) adds head-
              ers, footers, and page numbers; the result is  well  suited  for
              printing.

       ggeettttrrooffff
              Extract  the  raw  documentation from _S_C_R_I_P_T and process it with
              pandoc, converting it to troff code with man macros.  The  troff
              code  is then written to stdout.  The output is suitable for use
              with the man(1) command.

       ppddff    Extract the raw documentation from _S_C_R_I_P_T and  process  it  with
              pandoc to create a pdf document formatted for printing.  The pdf
              document is written to stdout.  This differs from the output  of
              mmaannppss  in  that  it  is  formatted  with a common LaTeX document
              style.

       ttttyy    Extract the raw documentation from _S_C_R_I_P_T and  process  it  with
              pandoc,  converting it to troff code with man macros.  The troff
              code is then run through groff.  This nicely formatted output is
              written  to stdout.  The output should resemble that of the pre-
              viously described mmaann format, the difference being that a  pager
              is  not  used.  This output is suitable for a, possibly verbose,
              usage text for a script.  The '-h' option to bbaasshhddoocc  uses  this
              for its output.

       mmdd     Extract  the raw markdown documentation from _S_C_R_I_P_T and write it
              in its raw unprocessed form to stdout.

       mmddss    Extract the raw documentation from _S_C_R_I_P_T, process it with  pan-
              doc,  then write a version of the documentation in a 'safe' fla-
              vor of markdown to stdout.  Why  would  this  be  useful?   Many
              online  code  repositories,  e.g.   Bitbucket, can automatically
              format and display  files  containing  markdown.   However,  the
              markdown  they  support  is  rudimentary at best, meaning that a
              document containing pandoc markdown may not  be  rendered  prop-
              erly.   This  option  attempts to transform the docstring into a
              'safe' flavor of markdown that it can be successfully  rendered.
              Some  useful  formatting may be lost in the process, but this is
              better than viewing impenetrable blocks  of  gibberish  on  such
              sites.

       The second argument, _S_C_R_I_P_T is a path to the bash script from which the
       documentation will be extracted.

   OOppttiioonnss
       --hh     Write help information to stdout.  This actually invokes bbaasshhddoocc
              on itself, requesting ttttyy format output.

       --vv     Write  the  version  string  to  stdout.  Semantic versioning is
              used.

   MMeettaa--iinnffoorrmmaattiioonn
       The first three lines of the  documentation  string  may  contain  meta
       information to be used by pandoc when creating output.  Those lines are
       in the format:

              % BASHDOC(1)
              % Kenneth East <http://east.fm>
              % December 13, 2015

       Their purpose is self-evident.  It is guaranteed not to render properly
       with  anything  other  than pandoc.  See the pandoc documentation[4] on
       meta-information for details.

   BBaasshh ppaarraammeetteerr eexxppaannssiioonn
       In here documents that begin with

               : <<'DOC'

       any shell variables within the here document are left as-is, i.e., they
       are not expanded.

       To  enable  parameter  substitution for shell variables, begin the doc-
       string with

               : <<DOC

       Note the absence of single quotes  around  DOC.   In  this  case,  bash
       parameter substitution is performed on the body of the here doc.

       In  the  latter  case,  note  that  if the docstring contains $(foo) or
       `foo`, it will be expanded by the shell, which will replace it with the
       output  of the foo command.  See the bash(1) man page for more informa-
       tion.

UUSSAAGGEE
   CCoommmmaanndd LLiinnee
       To see an example of what a documentation string looks like, simply run
       the command:

              $ bashdoc md `type -p bashdoc`

       The  above  command will extract the documentation string from the file
       which implements the bbaasshhddoocc command (i.e.,  the  file  containing  the
       documentation  string  which  generated  this document) and write it to
       stdout.

       To convert the same documentation string to a man page and then view it
       with man(8):

              $ bashdoc man `type -p bashdoc`

       To  create  a  printable  pdf version of the documentation for the bash
       script ~/bin/foo and then view it in OS-X preview:

              $ bashdoc pdf ~/bin/foo
              $ open foo.pdf

   AAss aa DDooccuummeennttaattiioonn PPrroovviiddeerr WWiitthhiinn OOtthheerr SSccrriippttss
       bbaasshhddoocc can also be used  as  a  documentation  provider  within  other
       scripts.   For example, if the script 'foo' is documented in the manner
       just described, bbaasshhddoocc can be called within 'foo'  to  produce  'foo's
       documentation.  E.g.:

              # Code snippet from within the 'foo' script.
              # OPT is a command line option to 'foo'

              case "$OPT" in
                  -h|--help)
                      # formatted documentation for 'foo' on stdout
                      bashdoc tty "$0"
                      ;;
                  --man)
                      # display formatted documentation for 'foo' with man(1)
                      bashdoc man "$0"
                      ;;
                  --md)
                      # raw documentation string for 'foo' on stdout
                      bashdoc md "$0"
                      ;;
              esac

   OOppttiinngg OOuutt
       If  you have a script with documentation formatted for use with bbaasshhddoocc
       but you do not wish to use bbaasshhddoocc, you can get the  raw  documentation
       on stdout with a one-liner:

                  $ S=.../path/to/script/to/get/doc/from/foo.sh
                  $ sed '/^:/,/^DOC/!d;s/^:/cat/' "$S" | sh -s

       This one line is actually the key to the entire bbaasshhddoocc scheme.  Thanks
       to mikeserv for the excellent idea.

EEXXIITT SSTTAATTUUSS
       bbaasshhddoocc exits with a status of 0 when successful  and  non-zero  other-
       wise.

IINNSSTTAALLLLAATTIIOONN
       Put the file _b_a_s_h_d_o_c somewhere in your path and make it executable.

       pandoc must be present on your system.  groff is required for mmaann, ggeett--
       ttrrooffff, and ttttyy.  LaTeX is required for ppddff.

BBUUGGSS
       No line in the docstring can begin with DOC in  the  first  column,  as
       this  will  terminate  the  here  document prematurely.  If you wish to
       embed a DOC in your docstring, one workaround is to precede the  embed-
       ded  DOC  with a space in column 1.  For an example of this workaround,
       inspect the docstring used to create this document.

       It can be difficult to create markdown that renders  satisfactorily  in
       both ppddff and in mmaann formats.

       More  specifically,  markdown  code  intended to render verbatim text /
       code fragments (e.g., `x = 33`) is rendered correctly by pandoc in pdf,
       as unadorned plain text in pandoc man pages, and surrounded by a shaded
       box on Bitbucket.  This is most unsatisfying.  Trying to get  satisfac-
       tory   rendering   in   pandoc's  pdf,  pandoc's  man  page,  and  Bit-
       bucket's/Github's markdown from a  single  document  can  be  extremely
       frustrating, if not impossible.  If something isn't clear in a mmaann ren-
       dering, it will likely be clarified with a ppddff rendering, which  should
       be more accurate.

       Single  quotes within the documentation string can confuse syntax high-
       lighing in editors and code formatters.

       Argument error handling is de-minimis.

NNOOTTEESS
       NN..BB.. This documentation was created with bbaasshhddoocc.

       The idea for this approach to embedding documentation in a shell script
       was    taken    from   mikeserv's   answer   in:   http://unix.stackex-
       change.com/questions/185657.

       This has only been tested on OS-X 10.11.2.

       It is a simple matter to add additional  pandoc  markdown  features  by
       extending the value of the '-f' option in this script's implementation.
       It would be nice to allow additional  extensions  to  be  enabled  from
       within the script being documented.

       Should  test  for  the  presence  of  pandoc,  groff, and LaTeX and act
       accordingly.

       Would be nice to allow for two classes of documentation  strings:  sum-
       mary  and  detailed.  This would allow a brief usage type output for -h
       along with a full version which would produce complete documentation.

       docopt uses semantic versioning.

       This documentation is amusingly lengthy given the brevity of the  asso-
       ciated code.

SSEEEE AALLSSOO
       pandoc, bash(1), groff(1), man(1)

       'socco.sh'  is  a more advanced documentation tool that prints comments
       and   code   side-by-side,    literate    programming    style.     See
       <http://rtomayko.github.io/shocco/>.

       Source    code    for    _b_a_s_h_d_o_c    is   available   at   <https://bit-
       bucket.org/MaDeuce/bashdoc>.

NNOOTTEESS
   [[11]]
       This is bbaasshhddoocc v2.0.0.

   [[22]]
       http://daringfireball.net/projects/markdown/syntax
       http://pandoc.org/README.html#pandocs-markdown

   [[33]]
       http://pandoc.org/README.html

   [[44]]
       http://pandoc.org/README.html#metadata-blocks

AAUUTTHHOORRSS
       Kenneth East <http://east.fm>.



                               December 15, 2015                    BASHDOC(1)
